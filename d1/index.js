// console.log("Hello World");

/*
	REPETITION CONTROL STRUCTURE (Loops)

	- Loops are one of the most important feature that a programming language must have.
	- It lets us execute code repeatedly in a pre-set number of time or maybe forever.

*/

/*
	Mini Activity:

	Create a function named greeting() and display a "Hello My World!" using console.log inside the function.

	Invoke the greeting() function 10 times.

	Take a screenshot of your work and send it to the batch hangouts.


*/

// function greeting(){
// 	console.log("Hello My World!");
// }

// // greeting();

// let count = 10;

// // contains an expression or condition that will be evaluated (true).
// 		//0 !== 0
// // false count !== 0
// while(count !== 0){
// 	console.log("This is printed inside the loop:" +count);
// 	greeting();

// 	//++ increased by 1
// 	//-- decreased by 1
// 	count--;
// }

// [SECTION] While Loop
// Allows to repeat an action or an instruction as long as the condition is true.
// Iteration it is the term given to the repetetion of statements

/*
	Syntax:
			//any unit of code that can be evaluated to a value. 
		while(expression/condition){
			//code block/statement
			
			//indicates how to advance the loop
			finalExpression++/--
		}

*/

// let count = 1;
// 	//while count is not greater than or equal to 5, the loop will run.
// 	 //6 <= 5 (false)
// while(count <= 5){
// 	// The current value of count is printed out every iteration.
// 	// console.log("While: " +count);
// 	console.log("Current value of count in the condition: "+count+" <= 5 is " +(count <=5));

// 	// increase the value of count by 1 after every iteration and will stop if the value of count reaches greater than 5.
// 	count++;
// }
				//6
// console.log(count);

// [SECTION] Do While Loop
// A do-while loop works a lot like the while loop. But unlike while loops do-while loops guarantee that the code will be excuted at least once.
/*
	do {
		//code block/statement

		++/--
	} while(expression/condition);

*/

// let count = 1;

// do{
// 	console.log("Do-while:" +count);
// 	count++;
// } while(count <= 5);
	
			// Can also convert boolean or date.
// let number = Number(prompt("Give a number:"));

// do {

// 	console.log("Do-while:" +number);
// 	// number++;
// 	// number = number + 1;
// 	number += 1;

// 	// loop will stop if the number is equal or greater than 10.
// } while(number < 10);

// [SECTION] For Loop
// A for loop is more flexible than while and do while. It consists of three parts:
/*
	Syntax:
		for(initialization; expression/condition; finalExpression++/--){
			//code block/statement;
		}
		
		- "initialization" contains the value that will be track in the progress of the loop.
		- "expression/condition" this will be evaluated to determine if the loop will run one more time.
		- "finalExpression" indicates how to advance the loop (++/--). 

*/

/*
	
	Mini Activity:
	Create a for loop that will start from 0 and will end at 20.

	Expected ouput:
	For Loop: 0
	For Loop: 1
	...
	For Loop: 19
	For Loop: 20

*/

// for(count = 0; count <= 20; count++){

// 	console.log("For loop:" +count);
// }

// Loops using String property

// let myString = "AngElito";

// .length property
// is used to count the characters of a string/variable.
// console.log(myString.length);

// Select individual characters
// Individual characters of string may be accessed using it's number index.
// console.log(myString[0]);
// console.log(myString[1]);
// console.log(myString[2]);
// console.log(myString[myString.length - 1]);

// Create a for loop that will print out the individual letters of the myString variable.
// for(x = 0; x < myString.length; x++){

// 	// Print the current value of myString character
// 	// myString[0] = T
// 	console.log("myString["+x+"]: " +myString[x]);

// }

// Create a loop that will print out the letters of the name individually and print out the number 3 instead of the vowel.

// let myName = '';

// for(i=0; i < myString.length; i++){
// 	if(myString[i].toLowerCase() == 'a' || myString[i].toLowerCase() == 'e'|| myString[i].toLowerCase() == 'i' || myString[i].toLowerCase() == 'o' || myString[i].toLowerCase() == 'u'){
// 		// console.log(3);
// 		// we add each character to a myName variable.
// 		// myName = myName + 3
// 		// for string "+=" is also used to concatenate
// 		myName += 3;
// 		console.log("This is myName inside if: " +myName);
// 	}
// 	else{
// 		// Print the consonant characters of the name.
// 		// console.log(myString[i]);
// 		// we add each character to a myName variable.
// 		// myName = myName + myString[i];
// 		myName += myString[i];
// 		console.log("This is myName inside else: " +myName);
// 	}
// }

// console.log(myName);

// [SECTION] Continue and Break

//"Continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all code block/statements.

// "Break" statement is used to terminate the current loop once a match found.

// Create a loop that if the value is divided by 2 and the remainder is 0, it will not print the number and continue to the next iteration. If the count value is greater than 10 it will stop the loop.

for(count = 0; count <= 20; count++){
		//check if the remainder is equal to zero. 
		// 2 % 2 == 0 
		//0 == 0 (true);
		// 3 % 2 (false)
		// will proceed to the next iteration
	if(count % 2 == 0){
		continue;
	}
		// 11 > 10 (true)
	if(count >= 10){
		// number values greater than 10 will no longer be printed.
		// this will stop the loop
		break;
	}

	console.log("Continue and Break: " +count);
}
